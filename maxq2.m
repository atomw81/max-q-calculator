%
% start date: october 2019
% Calculates the maximum dynamic pressure and some plots for zero zenith angles, i.e, vertical launch (for now),
% NOTE: this is a crude way of calculating and the inherent reduction in velocty due to the drag force is not considered
% Will update soon with the actual trajectory angles, drag force coefficients as the inputs and with more commenting
%-------------------------------------------------------------------------------------------%%
% Details
%--------
% Atmosphere model: ISA (max altitude = 84852m)
% inputs: (thrust, exhaust velocity, total initial mass, specific impulse) in SI units
% Enter the exhaust velocity (v_e) or specific impulse (i_sp) or both and enter for the other
% Example: maxq2(a,0,b,c)  or maxq2(a,b,c,0)
%         for Falcon 9 (without payload), run this:
%          maxq2(934000*9,0,549054,282) 
% change dt accordingly for finer results

function []= maxq2(th, v_e, m_i, i_sp)
%% initializing some constants
g_0=9.806651; Re=6348000;  timescale=500;Tp=288.15;Dp=1.225;
n=50000; dh=0;   dt=0.5; %dt=timescale/n;
n_div=round(timescale/dt);


%% calculating exhaust velocity and I_sp (needed later)
  if(v_e==0)
  {
    v_e=i_sp*g_0;
  }
  elseif (i_sp==0)
  {
  i_sp=v_e/g_0;
  }
  endif
b=th/v_e;
i=2;
t=linspace(0,timescale,n_div);
q=zeros(1,n_div);
v=zeros(1,n_div);
h=zeros(1,n_div);


%% acceleration expressions

g=@(H) (g_0/((1+H/Re)^2));
a=@(timE,lg) (v_e*b/(m_i-b*timE)-lg);
T(1)=0;
%% calculating...
while(h(i-1)<84852)
      dh=v(i-1)*dt;
      h(i)=h(i-1)+dh;  
      l_g=g(h(i-1));
      v(i)=v(i-1)+a(dt*(i-1),l_g)*dt;
      [Tp,Dp]=ISA(h(i-1));
      q(i)=0.5*Dp*(v(i-1))^2;
      T(i)=dt+T(i-1);
      i++;      
endwhile

T_el=i*dt;
maxQ=max(q);
k=find(q==maxQ);
[tm,dm]=ISA(h(k));
disp(sprintf('time (84852m): %2f s\nMaximum dynamic pressure: %2f Pa\nAt time = %2f s\nAt altitude = %2f m',T_el,maxQ,T(k),h(k)));
disp('velocity mag (m/s) and mach number:')
v(k)
v(k)/sqrt(1.4*287*tm)
figure(1)

plot(h(1,1:i-1),q(1,1:i-1));

xlabel('altitude (m)');
ylabel('dynamic pressure (Pa)');
figure(2)

plot(T(1,1:i-1),q(1,1:i-1))

xlabel('time (s)');
ylabel('dynamic pressure (Pa)');
        
end