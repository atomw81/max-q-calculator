# Max-q calculator

Calculates the maximum aerodynamic pressure (max-q) of the launch vehicle and also the altitude and the velocity of the launch vehicle at that instant. Inputs: Max thrust; specific impulse; effective exhaust velocity; initial lift-off mass.

Run maxq2.m file
